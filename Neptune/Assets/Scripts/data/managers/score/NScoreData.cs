﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
namespace neptune.data.managers.score {

    public class NScoreData {

        public int Type;
        public int MinValue;
        public int MaxValue;

        public NScoreData() {

        }

    }

}