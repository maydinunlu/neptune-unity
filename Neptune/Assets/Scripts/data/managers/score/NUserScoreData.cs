﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
using System.Xml.Serialization;

namespace neptune.data.managers.score {

    public class NUserScoreData {

        public int Type;
        public int Value;

        [XmlIgnore]
        public NScoreData ScoreData;

        public NUserScoreData() {

        }

    }

}