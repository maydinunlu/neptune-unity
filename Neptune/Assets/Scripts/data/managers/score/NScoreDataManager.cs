﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
using System;

namespace neptune.data.managers.score {

    public class NScoreDataManager : NDataManager {

        #region Events

        public delegate void IncreaseScoreEvent(NUserScoreData userScoreData);
        public event IncreaseScoreEvent OnIncreaseScore;

        public delegate void DecreaseScoreEvent(NUserScoreData userScoreData);
        public event DecreaseScoreEvent OnDecreaseScore;

        public delegate void SetScoreEvent(NUserScoreData userScoreData);
        public event SetScoreEvent OnSetScore;

        #endregion


        #region Collections

        public NDataCollection<NScoreData> DataCollection;
        public NUserDataCollection<NUserScoreData> UserDataCollection;

        #endregion


        #region Score: Increase, Decrease

        public void IncreaseScore(int type, int value) {
            NUserScoreData userScoreData = GetUserScoreData(type);
            userScoreData.Value += value;
            SetUserScoreData(userScoreData);

            if (OnIncreaseScore != null) {
                OnIncreaseScore(userScoreData);
            }
        }

        public void DecreaseScore(int type, int value) {
            NUserScoreData userScoreData = GetUserScoreData(type);
            userScoreData.Value -= value;
            SetUserScoreData(userScoreData);

            if (OnDecreaseScore != null) {
                OnDecreaseScore(userScoreData);
            }
        }

        private void SetUserScoreData(NUserScoreData userScoreData) {
            if (userScoreData.Value < userScoreData.ScoreData.MinValue) {
                userScoreData.Value = userScoreData.ScoreData.MinValue;

            } else if (userScoreData.Value > userScoreData.ScoreData.MaxValue) {
                userScoreData.Value = userScoreData.ScoreData.MaxValue;

            }

            OnSetUserScoreData(userScoreData);

            if (OnSetScore != null) {
                OnSetScore(userScoreData);
            }
        }

        protected virtual void OnSetUserScoreData(NUserScoreData userScoreData) {
            // Override
        }

        #endregion


        #region Get: Score, UserScoreData

        public int GetScore(int type) {
            NUserScoreData userScoreData = GetUserScoreData(type);
            if (userScoreData == null) {
                return 0;
            }
            return userScoreData.Value;
        }

        public NUserScoreData GetUserScoreData(int type) {
            NUserScoreData userScoreData = UserDataCollection.DataList.Find(x => x.Type == type);
            if (userScoreData == null) {
                //throw new Exception("UserScoreData is NULL! type: " + type);
            }
            return userScoreData;
        }

        #endregion

    }

}