﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
using System;

namespace neptune.data.managers {

    public class NDataManager {

        #region Init

        public void Init() {
            OnInit();
        }

        protected virtual void OnInit() {
            // Override
        }

        #endregion

        #region Load

        public void Load() {
            OnLoad();
        }

        protected virtual void OnLoad() {
            // Override
        }

        #endregion

        #region Save

        public void Save() {
            OnSave();
        }

        protected virtual void OnSave() {
            // Override
        }

        #endregion

        #region Reset

        public void Reset() {
            OnReset();
        }

        protected virtual void OnReset() {
            // Override
        }

        #endregion

    }

}