﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
using System.Collections.Generic;

namespace neptune.data.managers.purchase {

    public class NPurchaseAllowResult {

        public List<NPurchaseAllowData> ResultList = new List<NPurchaseAllowData>();
        public bool Result = true; // Default Value. Set to False from AddAllowData()

        public NPurchaseAllowResult() {

        }

        public void AddAllowData(NPurchaseAllowData allowData) {
            ResultList.Add(allowData);
            Result = false;
        }

    }

}