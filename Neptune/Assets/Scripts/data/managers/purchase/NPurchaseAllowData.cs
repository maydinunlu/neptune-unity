﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace neptune.data.managers.purchase {

    public class NPurchaseAllowData {

        public int Type;
        public int SubType;
        public bool Result;

        public NPurchaseAllowData() {

        }

        public NPurchaseAllowData(int type, int subType, bool result) {
            this.Type = type;
            this.SubType = subType;
            this.Result = result;
        }

    }

}