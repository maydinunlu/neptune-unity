﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
using System.Collections.Generic;

namespace neptune.data.managers.purchase {

    public abstract class NPurchaseDataManager : NDataManager {

        #region Buy

        public bool Buy(NPurchaseData price, bool showGetMore = true) {
            List<NPurchaseData> priceList = new List<NPurchaseData>();
            priceList.Add(price);

            bool result = Buy(priceList, showGetMore);
            return result;
        }

        public bool Buy(List<NPurchaseData> priceList, bool showGetMore = true) {
            return OnBuy(priceList, showGetMore);
        }

        protected abstract bool OnBuy(List<NPurchaseData> priceList, bool showGetMore = true);

        #endregion

        #region Allow: Buy

        public NPurchaseAllowResult AllowBuy(NPurchaseData price, bool showGetMore = true) {
            List<NPurchaseData> priceList = new List<NPurchaseData>();
            priceList.Add(price);

            return OnAllowBuy(priceList, showGetMore);
        }

        public NPurchaseAllowResult AllowBuy(List<NPurchaseData> priceList, bool showGetMore = true) {
            return OnAllowBuy(priceList, showGetMore);
        }

        protected abstract NPurchaseAllowResult OnAllowBuy(List<NPurchaseData> priceList, bool showGetMore = true);

        #endregion

    }

}