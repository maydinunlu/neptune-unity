﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace neptune.data.managers.purchase {

    public class NPurchaseData {

        public int Type;
        public int SubType;
        public int Value;

        public NPurchaseData() {

        }

    }

}