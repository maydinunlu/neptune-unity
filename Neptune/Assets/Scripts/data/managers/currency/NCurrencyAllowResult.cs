﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
using System.Collections.Generic;

namespace neptune.data.managers.currency {

    public class NCurrencyAllowResult {

        public List<NCurrencyAllowData> ResultList = new List<NCurrencyAllowData>();
        public bool Result = true; // Default Value. Set to False from AddAllowData()


        public NCurrencyAllowResult() {

        }

        public void AddAllowData(NCurrencyAllowData allowData) {
            ResultList.Add(allowData);
            Result = false;
        }

    }

}