﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
using System.Collections.Generic;

namespace neptune.data.managers.currency {

    public abstract class NCurrencyDataManager : NDataManager {

        #region Buy

        public bool Buy(NCurrencyData price, bool showGetMore = true) {
            List<NCurrencyData> priceList = new List<NCurrencyData>();
            priceList.Add(price);

            bool result = Buy(priceList, showGetMore);
            return result;
        }

        public bool Buy(List<NCurrencyData> priceList, bool showGetMore = true) {
            return OnBuy(priceList, showGetMore);
        }

        protected abstract bool OnBuy(List<NCurrencyData> priceList, bool showGetMore = true);

        #endregion

        #region Allow: Buy

        public NCurrencyAllowResult AllowBuy(NCurrencyData price, bool showGetMore = true) {
            List<NCurrencyData> priceList = new List<NCurrencyData>();
            priceList.Add(price);

            return OnAllowBuy(priceList, showGetMore);
        }

        public NCurrencyAllowResult AllowBuy(List<NCurrencyData> priceList, bool showGetMore = true) {
            return OnAllowBuy(priceList, showGetMore);
        }

        protected abstract NCurrencyAllowResult OnAllowBuy(List<NCurrencyData> priceList, bool showGetMore = true);

        #endregion

    }

}