﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
namespace neptune.data.managers.currency {

    public class NCurrencyData {

        public int Type;
        public int SubType;
        public int Value;

        public NCurrencyData() {

        }

    }

}