﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
namespace neptune.data.managers.currency {

    public class NCurrencyAllowData {

        public int Type;
        public int SubType;
        public bool Result;

        public NCurrencyAllowData() {

        }

        public NCurrencyAllowData(int type, int subType, bool result) {
            this.Type = type;
            this.SubType = subType;
            this.Result = result;
        }

    }

}