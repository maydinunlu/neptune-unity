﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
using System.Collections.Generic;
using System.Xml.Serialization;

namespace neptune.data {

    [XmlRoot("UserDataCollection")]
    public class NUserDataCollection<T> where T : class {

        public List<T> DataList = new List<T>();

        public NUserDataCollection() {

        }

    }

}