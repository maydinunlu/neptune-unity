﻿// Resource: http://wiki.unity3d.com/index.php?title=Saving_and_Loading_Data:_XmlSerializer
/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;

namespace neptune.data.providers {

    public static class NXmlDataProvider {

        #region Load

        public static T LoadFromResource<T>(this string path) where T : class, new() {
            var serializer = new XmlSerializer(typeof(T));

            T elements;

            TextAsset textAsset = Resources.Load<TextAsset>(path);
            if (textAsset != null) {
                XmlReader reader = XmlReader.Create(new StringReader(textAsset.text));
                elements = serializer.Deserialize(reader) as T;
                reader.Close();

            } else {
                //Debug.Log("File Not Found!");
                elements = new T();

            }

            return elements;
        }

        public static T LoadFromPersistent<T>(this string path) where T : class, new() {
            var serializer = new XmlSerializer(typeof(T));

            string filePath = GetPersistentFilePath(path + ".xml");

            T elements;

            if (File.Exists(filePath)) {
                XmlReader reader = XmlReader.Create(GetPersistentFilePath(path + ".xml"));
                elements = serializer.Deserialize(reader) as T;
                reader.Close();

                return elements;

            } else {
                //Debug.Log("File Not Found!");
                //elements = new T();
            }

            return null;
        }

        #endregion

        #region Save

        public static void Save<T>(string path, T t, bool isPersistent = true) where T : class {
            var serializer = new XmlSerializer(typeof(T));

            TextWriter writer = null;
            try {
                string filePath = (isPersistent) ? GetPersistentFilePath(path + ".xml") : (path + ".xml");

                writer = new StreamWriter(filePath, false);
                serializer.Serialize(writer, t);

            } finally {
                if (writer != null)
                    writer.Close();

            }
        }

        #endregion


        #region Get: PersistentFilePath

        private static string GetPersistentFilePath(string fileName) {
            string filePath = "";

#if UNITY_IPHONE
		        filePath = Application.persistentDataPath + "/" + fileName;

#elif UNITY_EDITOR
            filePath = Application.persistentDataPath + "/" + fileName;

#elif UNITY_ANDROID
		        filePath = Application.persistentDataPath + "/" + fileName;

#else
		        filePath = Application.dataPath + "/" + fileName;

#endif

            return filePath;
        }

        #endregion

    }

}