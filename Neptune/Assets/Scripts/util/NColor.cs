﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
using UnityEngine;

namespace neptune.util {

    public static class NColor {

        public static Color RGBToColor(float r, float g, float b, float a = 1) {
            return new Color((r / 255f), (g / 255f), (b / 255f), a);
        }

        public static Color SetAlpha(Color color, float alpha) {
            color.a = alpha;
            return color;
        }

    }

}