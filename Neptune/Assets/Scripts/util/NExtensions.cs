﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
using System;
using UnityEngine;

public static class NExtensions {

    /// <summary>
    /// 999999999 -> 999,999,999
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static string ToFormat(this int value) {
        return String.Format("{0:n0}", value);
    }

    /// <summary>
    /// Resouce: http://stackoverflow.com/questions/908543/how-to-convert-from-system-enum-to-base-integer
    /// </summary>
    /// <param name="enumValue"></param>
    /// <returns></returns>
    public static int ToInt(this Enum enumValue) {
        return (int)((object)enumValue);
    }

    /// <summary>
    /// Resource: http://answers.unity3d.com/questions/611850/destroy-all-children-of-object.html
    /// </summary>
    /// <param name="transform"></param>
    /// <returns></returns>
    public static Transform Clear(this Transform transform) {
        foreach (Transform child in transform) {
            GameObject.Destroy(child.gameObject);
        }
        return transform;
    }

    /// <summary>
    /// Resource: http://answers.unity3d.com/questions/830527/enumtryparse-method-not-support.html
    /// </summary>
    /// <typeparam name="TEnum"></typeparam>
    /// <param name="strEnumValue"></param>
    /// <returns></returns>
    public static TEnum ToEnum<TEnum>(this string strEnumValue) {
        return (TEnum)Enum.Parse(typeof(TEnum), strEnumValue);
    }


    public static T Next<T>(this T src) where T : struct {
        if (!typeof(T).IsEnum) throw new ArgumentException(String.Format("Argumnent {0} is not an Enum", typeof(T).FullName));

        T[] Arr = (T[])Enum.GetValues(src.GetType());
        int j = Array.IndexOf<T>(Arr, src) + 1;
        return (Arr.Length == j) ? Arr[0] : Arr[j];
    }

}