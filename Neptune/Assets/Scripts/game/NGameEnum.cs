﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace neptune.game {

    public enum NGameState {
        Running = 1,
        Paused = 2
    }

}