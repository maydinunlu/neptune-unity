﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
using neptune.core;
using UnityEngine;

namespace neptune.game {

    public class NGame : NBehaviour {

        #region State

        protected NGameState _state;
        public NGameState State {
            get { return _state; }
        }

        #endregion

        /**************************************************/

        #region OnAwake

        protected override void OnAwake() {
            _state = NGameState.Running;

            // Override: DontDestroyOnLoad(gameObject);
        }

        #endregion


        #region Game: StartNewGame

        public void StartNewGame() {
            OnStartNewGame();
        }

        protected virtual void OnStartNewGame() {
            // Override
        }

        #endregion

        #region Game: Continue

        public void ContinueGame() {
            OnContinueGame();
        }

        protected virtual void OnContinueGame() {
            // Override
        }

        #endregion

        #region Game: Pause

        private void OnApplicationPause(bool paused) {
            OnGamePause(paused);
        }

        protected virtual void OnGamePause(bool paused) {
            _state = (paused == true) ? NGameState.Paused : NGameState.Running;

            // Override
        }

        #endregion

        #region Game: Quit

        public void QuitGame() {
            Application.Quit();
        }

        private void OnApplicationQuit() {
            OnGameQuit();
        }

        protected virtual void OnGameQuit() {
            // Override
        }

        #endregion

    }

}