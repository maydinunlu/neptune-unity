﻿// Resource: http://pfonseca.com/swipe-detection-on-unity/
/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
using UnityEngine;

namespace neptune.interaction {

    public class NSwipe : MonoBehaviour {

        #region Events: NSwipeEvent

        public delegate void NSwipeEvent(NSwipeDirection direction);
        public event NSwipeEvent OnNSwipe;

        #endregion

        public float MaxSwipeTime = 0.5f;
        public float MinSwipeDistance = 50.0f;
        public bool AllowCheckSwipe = true;

        private float _fingerStartTime = 0.0f;
        private Vector2 _fingerStartPosition = Vector2.zero;

        private bool _isSwipe = false;


        #region Update

        void Update() {
            if (AllowCheckSwipe) {
                CheckSwipe();
            }
        }

        #endregion

        #region CheckSwipe

        private void CheckSwipe() {

            if (Input.touchCount > 0) {
                foreach (Touch touch in Input.touches) {
                    switch (touch.phase) {

                        #region TouchPhase: Began

                        case TouchPhase.Began: {
                                _isSwipe = true;
                                _fingerStartTime = Time.time;
                                _fingerStartPosition = touch.position;

                                break;
                            }

                        #endregion

                        #region TouchPhase: Canceled:

                        case TouchPhase.Canceled: {
                                _isSwipe = false;
                                break;
                            }

                        #endregion

                        #region TouchPhase: Ended

                        case TouchPhase.Ended: {
                                float gestureTime = (Time.time - _fingerStartTime);
                                float gestureDistance = (touch.position - _fingerStartPosition).magnitude;

                                if (_isSwipe && (gestureTime < MaxSwipeTime) && (gestureDistance > MinSwipeDistance)) {
                                    Vector2 direction = touch.position - _fingerStartPosition;
                                    Vector2 axis = GetAxis(direction);

                                    if (axis.x != 0.0f) {
                                        NSwipeDirection nDirection = (axis.x > 0.0f) ? NSwipeDirection.Right : NSwipeDirection.Left;
                                        if (OnNSwipe != null) {
                                            OnNSwipe(nDirection);
                                        }
                                    }

                                    if (axis.y != 0.0f) {
                                        NSwipeDirection nDirection = (axis.y > 0.0f) ? NSwipeDirection.Up : NSwipeDirection.Down;
                                        if (OnNSwipe != null) {
                                            OnNSwipe(nDirection);
                                        }
                                    }

                                }

                                break;
                            }

                            #endregion

                    }
                }
            }
        }

        private Vector2 GetAxis(Vector2 direction) {
            if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y)) { // Horizontal
                return (Vector2.right * Mathf.Sign(direction.x));
            }

            return (Vector2.up * Mathf.Sign(direction.y)); // Vertical
        }

        #endregion

    }

}