﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace neptune.interaction {

    public enum NSwipeDirection {
        None = 0,

        Up = 1,
        Down = 2,
        Left = 3,
        Right = 4
    }

}