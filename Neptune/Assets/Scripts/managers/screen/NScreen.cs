﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
using neptune.core;

namespace neptune.managers.screen {
    
    public class NScreen : NBehaviour {

        #region Visible

        protected bool _visible = false;
        public bool Visible {
            get { return _visible; }
        }

        #endregion

        /**************************************************/

        #region Show -> OnShow

        public void Show() {
            _visible = true;

            gameObject.SetActive(_visible);

            OnShow();
        }

        protected virtual void OnShow() {
            // Override
        }

        #endregion

        #region Hide -> OnHide

        public void Hide() {
            _visible = false;

            gameObject.SetActive(_visible);

            OnHide();
        }

        protected virtual void OnHide() {
            // Override
        }

        #endregion

    }

}