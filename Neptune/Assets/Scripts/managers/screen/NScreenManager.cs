﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
using System.Collections.Generic;

namespace neptune.managers.screen {

    public class NScreenManager : NManager {

        public List<NScreen> Screens;
        public NScreen CurrentScreen;

        private string _currentScreenName;

        /**************************************************/

        #region Screen: Get

        public NScreen GetScreen(string screenName) {
            return Screens.Find(x => x.name == screenName);
        }

        #endregion

        #region Screen: Show

        public void ShowScreen(string screenName) {
            _currentScreenName = screenName;

            NScreen newScreen = GetScreen(_currentScreenName);

            CurrentScreen.Hide();
            CurrentScreen = newScreen;

            newScreen.Show();
        }

        #endregion

        #region Screen: Hide

        public void HideScreen(string screenName) {
            NScreen screen = GetScreen(_currentScreenName);
            screen.Hide();
        }

        #endregion

    }

}