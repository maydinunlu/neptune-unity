﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description: Object Pooling
 *  Resources:
 *  https://github.com/SebLague/Object-Pooling
 *  https://www.youtube.com/watch?v=LhqP3EghQ-Q
 */
using System.Collections.Generic;
using UnityEngine;

namespace neptune.managers.poolObject {

    public class NPoolObjectManager : NManager {

        public GameObject PoolContainer;
        public List<NPoolObjectData> PoolObjectDataList = new List<NPoolObjectData>();

        protected Dictionary<int, Queue<NPoolObject>> _poolObjectDic = new Dictionary<int, Queue<NPoolObject>>();

        /**************************************************/

        #region Pool: Create

        public void CreatePool(NPoolObject poolObjectPrefab, int poolSize) {
            int poolObjectId = poolObjectPrefab.GetInstanceID();

            if (!_poolObjectDic.ContainsKey(poolObjectId)) {
                _poolObjectDic.Add(poolObjectId, new Queue<NPoolObject>());

                GameObject poolObjectContainer = new GameObject("Pool_" + poolObjectPrefab.name + "_[" + poolSize + "]");
                poolObjectContainer.transform.parent = PoolContainer.transform;

                for (int i = 0; i < poolSize; i++) {
                    NPoolObject poolObject = Instantiate(poolObjectPrefab) as NPoolObject;
                    poolObject.gameObject.transform.parent = poolObjectContainer.transform;
                    poolObject.gameObject.SetActive(false);

                    _poolObjectDic[poolObjectId].Enqueue(poolObject);
                }
            }
        }

        #endregion

        #region Object: Reuse

        public NPoolObject ReuseObject(NPoolObject poolObjectPrefab) {
            int poolObjectId = poolObjectPrefab.GetInstanceID();

            if (_poolObjectDic.ContainsKey(poolObjectId)) {
                NPoolObject poolObject = _poolObjectDic[poolObjectId].Dequeue();
                _poolObjectDic[poolObjectId].Enqueue(poolObject);

                poolObject.gameObject.SetActive(true);

                return poolObject;
            }

            return null;
        }

        #endregion

    }

}