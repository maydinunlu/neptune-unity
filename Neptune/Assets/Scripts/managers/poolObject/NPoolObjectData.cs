﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
using System;

namespace neptune.managers.poolObject {

    [Serializable]
    public class NPoolObjectData {

        public NPoolObject PoolObjectPrefab;
        public int PoolSize;

        public NPoolObjectData() {

        }

    }

}