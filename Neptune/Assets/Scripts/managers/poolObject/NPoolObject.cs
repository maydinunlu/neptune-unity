﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
using neptune.core;

namespace neptune.managers.poolObject {

    public class NPoolObject : NBehaviour {

        #region ReuseObject -> OnReuseObject

        public void ReuseObject() {
            OnReuseObject();
        }

        protected virtual void OnReuseObject() {
            // Overide
        }

        #endregion

        #region DestroyObject -> OnDestroyObject

        public void DestroyObject(bool destroy = false) {
            gameObject.SetActive(false);

            OnDestroyObject(destroy);

            if (destroy) {
                Destroy(gameObject);
            }
        }

        protected virtual void OnDestroyObject(bool destroy) {
            // Overide
        }

        #endregion

    }

}