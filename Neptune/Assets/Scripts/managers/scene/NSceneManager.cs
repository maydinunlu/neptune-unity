﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
using unity = UnityEngine.SceneManagement;

namespace neptune.managers.scene {

    public class NSceneManager : NManager {

        #region CurrentSceneName

        private string _currentSceneName;
        public string CurrentSceneName {
            get { return _currentSceneName; }
        }

        #endregion

        /**************************************************/

        #region LoadScene

        public void LoadScene(string sceneName) {
            _currentSceneName = sceneName;

            unity.SceneManager.LoadScene(_currentSceneName);
        }

        #endregion

    }

}