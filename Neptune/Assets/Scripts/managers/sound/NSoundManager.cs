﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
using System.Collections.Generic;
using UnityEngine;

namespace neptune.managers.sound {

    public abstract class NSoundManager : NManager {

        public List<AudioSource> Sounds;

        /**************************************************/

        public void Play(string name, float delay = 0) {
            if (OnAllowPlay()) {
                OnPlay(name, delay);
            }
        }

        protected abstract bool OnAllowPlay();

        protected virtual void OnPlay(string name, float delay = 0f) {
            AudioSource sound = Sounds.Find(x => x.name == name);
            if (sound != null) {
                if (delay == 0) {
                    sound.Play();
                } else {
                    sound.PlayDelayed(delay);
                }
            }
        }

    }

}