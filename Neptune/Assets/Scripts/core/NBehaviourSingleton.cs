﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
using UnityEngine;

namespace neptune.core {

    public class NBehaviourSingleton<T> : MonoBehaviour where T : NBehaviourSingleton<T> {

        private static T _instance;
        public static T Instance {
            get {
                return _instance;
            }
        }

        public static bool IsInitialized {
            get {
                return _instance != null;
            }
        }


        #region Awake

        void Awake() {
            if (_instance != null) {
                Debug.LogErrorFormat("Trying to instantiate a second instance of singleton class {0}", GetType().Name);

            } else {
                _instance = (T)this;

            }

            OnAwake();
        }

        protected virtual void OnAwake() {
            // Override
        }

        #endregion

        #region Start

        void Start() {
            OnStart();
        }

        protected virtual void OnStart() {
            // Override
        }

        #endregion


        #region OnDestroy

        protected virtual void OnDestroy() {
            if (_instance == this) {
                _instance = null;
            }
        }

        #endregion

    }

}