﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace neptune.core {

    public class NSingleton<T> where T : new() {

        private static T _instance;
        public static T Instance {
            get {
                if (_instance == null) {
                    _instance = new T();
                }
                return _instance;
            }
        }

        public static bool IsInitialized {
            get {
                return _instance != null;
            }
        }

    }

}