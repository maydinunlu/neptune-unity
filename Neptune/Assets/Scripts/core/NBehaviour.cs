﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
using UnityEngine;

namespace neptune.core {

    public class NBehaviour : MonoBehaviour {

        #region Awake

        void Awake() {
            OnAwake();
        }

        protected virtual void OnAwake() {
            // Override
        }

        #endregion

        #region Start

        void Start() {
            OnStart();
        }

        protected virtual void OnStart() {
            // Override
        }

        #endregion

    }

}