﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
using UnityEngine;

namespace neptune.assets {

    public class NAsset {

        private static float _defaultPPU;

        public static readonly Vector2 PIVOT_CENTER = new Vector2(0.5f, 0.5f);

        /**************************************************/

        #region Init

        public static void Init() {
            Init(100);
        }

        public static void Init(int ppu) {
            _defaultPPU = ppu;
        }

        #endregion


        #region GetSprite: FromPath

        public static Sprite GetSpriteFromPath(string path) {
            return GetSpriteFromPath(path, PIVOT_CENTER, _defaultPPU);
        }

        public static Sprite GetSpriteFromPath(string path, float ppu) {
            return GetSpriteFromPath(path, PIVOT_CENTER, ppu);
        }

        public static Sprite GetSpriteFromPath(string path, Vector2 pivot) {
            return GetSpriteFromPath(path, pivot, _defaultPPU);
        }

        public static Sprite GetSpriteFromPath(string path, Vector2 pivot, float ppu) {
            Texture2D texture = GetAsset<Texture2D>(path);

            var rect = new Rect(0, 0, texture.width, texture.height);
            Sprite sprite = Sprite.Create(texture, rect, pivot, ppu);
            return sprite;
        }

        #endregion

        #region GetSprite: FromTexture

        public static Sprite GetSpriteFromTexture(Texture2D texture) {
            return GetSpriteFromTexture(texture, PIVOT_CENTER, _defaultPPU);
        }

        public static Sprite GetSpriteFromTexture(Texture2D texture, float ppu) {
            return GetSpriteFromTexture(texture, PIVOT_CENTER, ppu);
        }

        public static Sprite GetSpriteFromTexture(Texture2D texture, Vector2 pivot) {
            return GetSpriteFromTexture(texture, pivot, _defaultPPU);
        }

        public static Sprite GetSpriteFromTexture(Texture2D texture, Vector2 pivot, float ppu) {
            var rect = new Rect(0, 0, texture.width, texture.height);
            Sprite sprite = Sprite.Create(texture, rect, pivot, ppu);
            return sprite;
        }

        #endregion


        #region Get: Asset

        public static T GetAsset<T>(string path) where T : UnityEngine.Object {
            try {
                return (T)Resources.Load(path);

            } catch {
                Debug.LogWarning("Asset not found! \"" + path + "\"");
                return null;
            }
        }

        #endregion

    }

}