﻿using neptune.core;
using UnityEngine.UI;

public class NPopupOverlay : NBehaviour {

    #region Childs

    public Image ImgOverlay;

    #endregion

    #region Visible

    protected bool _visible = false;
    public bool Visible {
        get { return _visible; }
    }

    #endregion

    /**************************************************/

    #region SetVisible

    public void SetVisible(bool visible) {
        _visible = visible;

        gameObject.SetActive(_visible);
    }

    #endregion

}