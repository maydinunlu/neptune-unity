﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
using DG.Tweening;
using neptune.ui.core;
using neptune.util;
using UnityEngine;

namespace neptune.ui.popup {

    public class NPopupManager : NUIManager {

        #region Overlay

        public NPopupOverlay Overlay;

        #endregion


        /**************************************************/


        #region OnStart

        protected override void OnStart() {
            base.OnStart();

            Init<NPopup>();
        }

        #endregion


        #region Show -> [ShowRequest] -> OnShowRequest

        public void Show(string name, object data = null) {
            ShowRequest<NPopup>(name, data);
        }

        protected override void OnShowRequest<T>(T ui) {
            //TODO: Add Queue Structure
            if (_openUIList.Contains(ui)) {
                return;
            }

            InitUIPosition(ui);
            ShowAccept<T>(ui);
            PlayShowAnim(ui as NPopup);

            if (_openUIList.Count > 0) {
                Overlay.SetVisible(true);
                SetOverlayAlpha((ui as NPopup).ShowWithOverlay);
                RefreshOverlaySiblingIndex(ui);
            }

        }

        #endregion

        #region Hide -> [HideRequest] -> OnHideRequest

        public void Hide(NPopup view) {
            HideRequest<NPopup>(view);
        }

        protected override void OnHideRequest<T>(T ui) {
            base.OnHideRequest(ui);

            if (_openUIList.Count == 1) {
                HideOverlay();
            }

            PlayHideAnim(ui as NPopup);
        }

        #endregion


        #region Play: ShowAnim

        private void PlayShowAnim(NPopup popup) {
            popup.transform.DOMove(Vector3.zero, .35f)
                .OnComplete(() => OnPlayShowAnimComplete(popup))
                .SetEase(Ease.OutQuint);
        }

        private void OnPlayShowAnimComplete(object objUI) {
            NPopup popup = objUI as NPopup;
            popup.ShowComplete();
            // You can use this structure and do something about popup own logic
        }

        #endregion

        #region Play: HideAnim

        private void PlayHideAnim(NPopup popup) {
            Rect uiRect = popup.GetComponent<RectTransform>().rect;
            float newY = (_canvasSizeDelta.y * .5f) + (uiRect.height * .5f);
            Vector3 targetPos = new Vector2(0, newY);

            popup.transform.DOMove(targetPos, .15f)
                .OnComplete(() => OnPlayHideAnimComplete(popup))
                .SetEase(Ease.OutQuint);
        }

        private void OnPlayHideAnimComplete(object objUI) {
            NPopup popup = objUI as NPopup;

            HideAccept(popup);
            
            if (_openUIList.Count == 0) {
                HideOverlay();
            } else {
                RefreshOverlaySiblingIndex(_openUIList[_openUIList.Count-1]);
            }
        }

        #endregion


        #region Init: UIPosition

        private void InitUIPosition(NUI ui) {
            Rect uiRect = ui.GetComponent<RectTransform>().rect;
            Vector3 newPos = ui.transform.localPosition;

            newPos.y = (_canvasSizeDelta.y * .5f) + (uiRect.height * .5f);
            ui.transform.localPosition = newPos;
        }

        #endregion


        #region Overlay: Show

        private void SetOverlayAlpha(bool showWithOverlay) {
            Color color = showWithOverlay ? NColor.SetAlpha(Overlay.ImgOverlay.color, .75f) : NColor.SetAlpha(Overlay.ImgOverlay.color, 0);
            Overlay.ImgOverlay.color = color;
        }

        #endregion

        #region Overlay: Hide

        private void HideOverlay() {
            if (Overlay != null) {
                Overlay.gameObject.transform.SetSiblingIndex(0);
                Overlay.SetVisible(false);
            }
        }

        #endregion

        #region Overlay: RefreshSiblingIndex

        private void RefreshOverlaySiblingIndex<T>(T ui) where T : NUI {
            int overlaySiblingIndex = ui.gameObject.transform.GetSiblingIndex() - 1;
            if (overlaySiblingIndex < 0) {
                overlaySiblingIndex = 0;
            }
            Overlay.gameObject.transform.SetSiblingIndex(overlaySiblingIndex);
        }

        #endregion

    }

}