﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
using neptune.ui.core;
using UnityEngine;

namespace neptune.ui.view {

    public class NViewManager : NUIManager {

        #region OnStart

        protected override void OnStart() {
            base.OnStart();

            Init<NView>();
        }

        #endregion


        #region Show -> [ShowRequest] -> OnShowRequest

        public void Show(string name, object data = null) {
            ShowRequest<NView>(name, data);
        }

        protected override void OnShowRequest<T>(T ui) {
            ShowAccept<T>(ui);
        }

        #endregion

        #region Hide -> [HideRequest] -> OnHideRequest

        public void Hide(NView view) {
            HideRequest<NView>(view);
        }

        protected override void OnHideRequest<T>(T ui) {
            HideAccept(ui);
        }

        #endregion

    }

}