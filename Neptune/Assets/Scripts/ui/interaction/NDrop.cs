﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 *  Resource: https://www.youtube.com/watch?v=c47QYgsJrWc
 */
using neptune.core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace neptune.ui.interaction {

    public class NDrop : NBehaviour, IDropHandler {

        #region Events: (OnNDropEvent)

        public delegate void OnNDropEvent(NDrop nDrop, NDrag nDrag);
        public event OnNDropEvent OnNDrop;

        #endregion


        #region NDrag

        private NDrag _nDrag;

        #endregion


        #region DropContainer

        public GameObject DropContainer;

        #endregion


        /**************************************************/


        #region Interaction: OnDrop (IDropHandler)

        public void OnDrop(PointerEventData eventData) {
            _nDrag = eventData.pointerDrag.GetComponent<NDrag>();
            _nDrag.OnCancel = true;

            if (OnNDrop != null) {
                OnNDrop(this, _nDrag);
            }
        }

        #endregion

    }

}