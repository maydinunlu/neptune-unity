﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 *  Resource: https://www.youtube.com/watch?v=c47QYgsJrWc
 */
using neptune.core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace neptune.ui.interaction {

    public class NDrag : NBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

        #region Events: (OnNDragStartEvent, OnNDragEvent)

        public delegate void OnNDragStartEvent(NDrag nDrag);
        public event OnNDragStartEvent OnNDragStart;

        public delegate void OnNDragEvent(NDrag nDrag);
        public event OnNDragEvent OnNDrag;

        #endregion


        #region CanvasGroup

        private CanvasGroup _canvasGroup;

        #endregion


        #region Item

        private GameObject _item;

        #endregion

        #region Parent

        private Transform _initParent;

        #endregion


        #region Position: (Init, New)

        private Vector3 _initPosition;
        private Vector3 _newPosition;

        #endregion


        #region Allow: Drag

        public bool AllowDrag = true;

        #endregion

        #region OnCancel

        public bool OnCancel = true;
        
        #endregion


        #region Data

        public object Data;

        #endregion


        /**************************************************/


        #region OnAwake

        protected override void OnAwake() {
            base.OnAwake();

            Init();
        }

        #endregion

        #region Init

        private void Init() {
            _canvasGroup = this.gameObject.AddComponent<CanvasGroup>();
        }

        #endregion


        #region Interaction: OnBeginDrag (IBeginDragHandler)

        public void OnBeginDrag(PointerEventData eventData) {
            if (!AllowDrag) {
                return;
            }

            #region Set: Item

            _item = this.gameObject;

            #endregion

            #region Set: InitParent

            _initParent = transform.parent;

            #endregion

            #region Set: InitPosition

            _initPosition = transform.position;

            #endregion

            _canvasGroup.blocksRaycasts = false;

            if (OnNDragStart != null) {
                OnNDragStart(this);
            }
        }

        #endregion

        #region Interaction: OnDrag (IDragHandler)

        public void OnDrag(PointerEventData eventData) {
            if (!AllowDrag) {
                return;
            }

            #region Set: NewPosition

            _newPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            _newPosition.z = 999;

            transform.position = _newPosition;

            #endregion

            if (OnNDrag != null) {
                OnNDrag(this);
            }
        }

        #endregion

        #region Interaction: OnEndDrag (IEndDragHandler)

        public void OnEndDrag(PointerEventData eventData) {
            if (OnCancel) {
                CancelDrop();
            }
            
        }

        #endregion


        #region Drop: Cancel

        public void CancelDrop() {
            _item = null;

            if (transform.parent == _initParent) {
                transform.position = _initPosition;
            }

            _canvasGroup.blocksRaycasts = true;

            OnCancel = true;
        }

        #endregion

        #region Drop: Accept

        public void AcceptDrop(Transform newParent) {
            _item.transform.SetParent(newParent, true);
            _item.transform.localPosition = Vector3.zero;
        }

        #endregion

    }

}