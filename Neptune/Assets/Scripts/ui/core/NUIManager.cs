﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
using neptune.core;
using System.Collections.Generic;
using UnityEngine;

namespace neptune.ui.core {

    public class NUIManager : NBehaviour {

        #region List: RegisterDataList

        public List<NUIRegisterData> RegisterDataList = new List<NUIRegisterData>();

        #endregion

        #region List: WaitingUIList

        protected List<NUI> _waitingUIList = new List<NUI>();

        #endregion

        #region List: OpenUIList

        protected List<NUI> _openUIList = new List<NUI>();

        #endregion

        #region Canvas

        public Canvas Canvas;
        protected RectTransform _canvasRectTransform;
        protected Vector2 _canvasSizeDelta;

        #endregion


        /**************************************************/


        #region OnStart

        protected override void OnStart() {
            base.OnStart();

            _canvasRectTransform = Canvas.GetComponent<RectTransform>();
            _canvasSizeDelta = _canvasRectTransform.sizeDelta;
        }

        #endregion


        #region Init

        protected void Init<T>() where T : NUI {
            foreach (NUIRegisterData registerData in RegisterDataList) {
                if (registerData.ShowOnStart) {
                    ShowRequest<T>(registerData.NUI.name);
                }
            }
        }

        #endregion


        #region Create: UI

        protected T CreateUI<T>(NUIRegisterData registerData) where T : NUI {
            if (registerData.CreationMode == UICreationMode.Instance) {
                return registerData.NUI as T;
            }
            return Instantiate(registerData.NUI) as T;
        }

        #endregion


        #region Show Process: ShowRequest -> OnShowRequest -> ShowAccept

        protected void ShowRequest<T>(string name, object data = null) where T : NUI {
            NUIRegisterData registerData = GetRegisterData(name);

            T ui = CreateUI<T>(registerData);
            ui.SetRegisterData(registerData);
            if (registerData.Parent != null) {
                ui.SetParent(registerData.Parent);
            }

            ui.SetData(data);

            OnShowRequest(ui);
        }

        protected virtual void OnShowRequest<T>(T ui) where T : NUI {
            // Override
        }

        protected void ShowAccept<T>(T ui) where T : NUI {
            ui.Show();
            _openUIList.Add(ui);
        }

        #endregion

        #region Hide Process: HideRequest -> OnHideRequest -> HideAccept

        protected void HideRequest<T>(T ui) where T : NUI {
            OnHideRequest(ui);
        }

        protected virtual void OnHideRequest<T>(T ui) where T : NUI {
            // Override
        }

        protected void HideAccept<T>(T ui) where T : NUI {
            _openUIList.Remove(ui);
            ui.Hide();

            if (ui.RegisterData.CreationMode == UICreationMode.Prefab) {
                Destroy(ui.gameObject);
            }
        }

        #endregion


        #region Get: UI

        public T GetUI<T>() where T : NUI {
            T view = null;
            view = GetRegisterData(typeof(T).Name).NUI as T;
            return view;
        }

        #endregion

        #region Get: RegisterData

        public NUIRegisterData GetRegisterData(string name) {
            return RegisterDataList.Find(x => x.NUI.name.ToLower() == name.ToLower());
        }

        #endregion

        #region Get: WaitingCount

        public int GetWaitingCount() {
            return _waitingUIList.Count;
        }

        #endregion

        #region Get: OpenCount

        public int GetOpenCount() {
            return _openUIList.Count;
        }

        #endregion

    }

}