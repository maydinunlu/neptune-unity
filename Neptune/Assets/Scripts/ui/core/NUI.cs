﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
using neptune.core;
using UnityEngine;

namespace neptune.ui.core {

    public class NUI : NBehaviour {

        #region RegisterData

        protected NUIRegisterData _registerData;
        public NUIRegisterData RegisterData {
            get { return _registerData; }
        }

        #endregion

        #region Data

        protected object _data;
        public object Data {
            get { return _data; }
        }

        #endregion

        #region Visible

        protected bool _visible = false;
        public bool Visible {
            get { return _visible; }
        }

        #endregion


        /**************************************************/


        #region Show -> OnShow -> ShowComplete -> OnShowComplete

        public void Show() {
            _visible = true;

            gameObject.SetActive(_visible);

            OnShow();
        }

        protected virtual void OnShow() {
            // Override
        }

        public void ShowComplete() {
            OnShowComplete();
        }

        protected virtual void OnShowComplete() {
            // Override
        }

        #endregion


        #region Hide -> OnHide

        public void Hide() {
            _visible = false;

            gameObject.SetActive(_visible);

            OnHide();
        }

        protected virtual void OnHide() {
            // Override
        }

        #endregion


        #region SetRegisterData

        public void SetRegisterData(NUIRegisterData registerData) {
            this._registerData = registerData;
        }

        #endregion

        #region SetData -> OnSetData

        public void SetData(object data) {
            this._data = data;

            OnSetData(data);
        }

        protected virtual void OnSetData(object data) {
            // Override
        }

        #endregion


        #region SetParent

        public void SetParent(GameObject parent) {
            this.transform.SetParent(parent.transform, false);
        }

        #endregion

        #region SetVisible

        public void SetVisible(bool visible) {
            _visible = visible;

            gameObject.SetActive(_visible);
        }

        #endregion

    }

}