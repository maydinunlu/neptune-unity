﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
using System;
using UnityEngine;

namespace neptune.ui.core {

    [Serializable]
    public class NUIRegisterData {

        public NUI NUI;
        public GameObject Parent;

        public UICreationMode CreationMode = UICreationMode.Instance;

        public bool ShowOnStart = false;
        public bool IsSingleton = false;

    }

}