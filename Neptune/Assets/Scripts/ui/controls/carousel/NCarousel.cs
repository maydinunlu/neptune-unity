﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace neptune.ui.controls.carousel {

    [RequireComponent(typeof(Mask))]
    [RequireComponent(typeof(ScrollRect))]
    public abstract class NCarousel : NControl {

        #region Prefabs: ItemPrefab

        public NCarouselItem ItemPrefab;

        #endregion


        #region Childs: ItemContainer

        public NCarouselItemContainer ItemContainer;

        #endregion


        #region Item: (ItemList, SelectedItem)

        protected List<NCarouselItem> _itemList;
        public List<NCarouselItem> ItemList {
            get {
                return _itemList;
            }
        }

        protected NCarouselItem _selectedItem;
        public NCarouselItem SelectedItem {
            get {
                return _selectedItem;
            }
        }

        #endregion


        /**************************************************/


        #region ItemList: Create

        protected void CreateItemList<D, T>(D dataList) where D : List<T> {
            if (dataList == null) {
                return;
            }

            Clear();

            _itemList = new List<NCarouselItem>(dataList.Count);

            foreach (T data in dataList) {
                AddItem<T>(data);
            }
        }

        #endregion


        #region Item: (Add, Remove)

        protected abstract void AddItem<D>(D data);
        protected abstract void RemoveItem<T>(T item) where T : NCarouselItem;

        #endregion


        #region Clear

        public void Clear() {
            if (_itemList == null) {
                return;
            }

            _selectedItem = null;
            foreach (NCarouselItem item in _itemList) {
                RemoveItem(item);
            }
            ItemContainer.Clear();
        }

        #endregion

    }

}