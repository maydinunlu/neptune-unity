﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
using neptune.core;
using UnityEngine;
using UnityEngine.UI;

namespace neptune.ui.controls.carousel {

    [RequireComponent(typeof(ContentSizeFitter))]
    public class NCarouselItemContainer : NBehaviour {

        #region Clear

        public void Clear() {
            transform.Clear();
        }

        #endregion

    }

}