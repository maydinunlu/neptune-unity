﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
using neptune.core;
using UnityEngine;
using UnityEngine.UI;

namespace neptune.ui.controls.carousel {

    [RequireComponent(typeof(HorizontalLayoutGroup))]
    public class NCarouselItemContainerHorizontal : NCarouselItemContainer {

    }

}