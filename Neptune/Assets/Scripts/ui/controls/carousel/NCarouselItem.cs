﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
using neptune.core;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace neptune.ui.controls.carousel {

    [RequireComponent(typeof(LayoutElement))]
    public class NCarouselItem: NBehaviour {

        #region Event: ItemSelect

        public event Action<NCarouselItem> ItemSelectEvent;

        #endregion


        /**************************************************/


        #region Event: OnItemSelect

        protected void OnItemSelect(NCarouselItem item) {
            if (ItemSelectEvent != null) {
                ItemSelectEvent(item);
            }
        }

        #endregion

    }

}