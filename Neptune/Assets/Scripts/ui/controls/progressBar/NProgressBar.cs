﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
using neptune.util;
using UnityEngine;

namespace neptune.ui.controls.progressBar {

    public class NProgressBar : NControl {

        public GameObject Bar;
        private RectTransform _barRect;

        private int _value;
        public int Value {
            get {
                return _value;
            }
        }
        private int _minValue = 0;
        private int _maxValue = 100;

        private float _initialBarX;

        /**************************************************/

        #region Init

        public void Init(int minValue, int maxValue) {
            _minValue = minValue;
            _maxValue = maxValue;

            _barRect = Bar.GetComponent<RectTransform>();
            SetInitialBarX();
        }

        #endregion

        #region SetInitialBarX

        private void SetInitialBarX() {
            _initialBarX = -(_barRect.sizeDelta.x);
        }

        #endregion

        #region Set: Value

        public void SetValue(int targetValue) {
            if (targetValue < _minValue) {
                targetValue = _minValue;
            } else if (targetValue > _maxValue) {
                targetValue = _maxValue;
            }
            _value = targetValue;

            int targetPercent = NMath.GetPercentByMaxAndMin(targetValue, _minValue, _maxValue);
            int currentPercent = NMath.GetPercentByMaxAndMin(_value, _minValue, _maxValue);

            float barX = _initialBarX - ((currentPercent * _initialBarX) / 100);
            float targetBarX = _initialBarX - ((targetPercent * _initialBarX) / 100); // _bar.width -> _maxValue; gibi falan filan

            Vector2 newPos = new Vector2(targetBarX, _barRect.transform.localPosition.y);
            _barRect.anchoredPosition = newPos;
        }

        #endregion

    }

}