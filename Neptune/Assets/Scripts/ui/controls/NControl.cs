﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
using neptune.core;

namespace neptune.ui.controls {

    public class NControl : NBehaviour {

        #region Data

        protected object _data;
        public object Data {
            get { return _data; }
        }

        #endregion

        #region Visible

        protected bool _visible = false;
        public bool Visible {
            get { return _visible; }
        }

        #endregion

        /**************************************************/

        #region SetData -> OnSetData

        public void SetData(object data) {
            this._data = data;

            OnSetData(data);
        }

        protected virtual void OnSetData(object data) {
            // Override
        }

        #endregion

        #region Init -> OnInit

        public void Init() {
            OnInit();
        }

        protected virtual void OnInit() {
            // Override
        }

        #endregion


        #region SetVisible

        public void SetVisible(bool visible) {
            _visible = visible;

            gameObject.SetActive(_visible);
        }

        #endregion

    }

}