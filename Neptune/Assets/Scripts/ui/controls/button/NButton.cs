﻿using neptune.core;
using UnityEngine.UI;

namespace ui.controls.button {

    public class NButton : NBehaviour {

        public Button Btn;
        public Image ImgDisable;

        private bool _disable = false;
        public bool Disable {
            get {
                return _disable;
            }
        }

        /**************************************************/

        #region Set: Disable

        public void SetDisable(bool value) {
            Btn.interactable = !value;
            ImgDisable.gameObject.SetActive(value);
        }

        #endregion

    }

}