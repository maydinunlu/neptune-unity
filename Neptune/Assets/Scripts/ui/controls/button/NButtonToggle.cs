﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
using neptune.ui.controls.toggle;
using UnityEngine.Events;
using UnityEngine.UI;

namespace neptune.ui.controls.button {

    public class NButtonToggle : NToggle {

        public Button BtnNormal;
        public Button BtnSelected;

        public Image ImgDisable;

        private bool _disable = false;
        public bool Disable {
            get {
                return _disable;
            }
        }

        /**************************************************/

        #region OnAwake

        protected override void OnAwake() {
            base.OnAwake();

            AddEvents();
        }

        #endregion

        #region OnStart

        protected override void OnStart() {
            base.OnStart();

            OnSetSelected(_selected, false);
        }

        #endregion


        #region OnSetSelected

        protected override void OnSetSelected(System.Boolean value, bool dispatchEvent) {
            base.OnSetSelected(value, dispatchEvent);

            _selected = value;

            BtnNormal.gameObject.SetActive(!_selected);
            BtnSelected.gameObject.SetActive(_selected);
        }

        #endregion

        #region OnSetInteractible

        protected override void OnSetInteractible(System.Boolean value) {
            BtnNormal.interactable = value;
            BtnSelected.interactable = value;

            if (ImgDisable != null) {
                ImgDisable.gameObject.SetActive(!value);
            }
        }

        #endregion


        #region Event: OnBtnNormalClick

        private void OnBtnNormalClick() {
            SetSelected(true);
        }

        #endregion

        #region Event: OnBtnSelectedClick

        private void OnBtnSelectedClick() {
            SetSelected(false);
        }

        #endregion


        #region Events: (Add, Remove)

        private void AddEvents() {
            BtnNormal.onClick.AddListener(new UnityAction(OnBtnNormalClick));
            BtnSelected.onClick.AddListener(new UnityAction(OnBtnSelectedClick));
        }

        private void RemoveEvents() {
            BtnNormal.onClick.RemoveListener(OnBtnNormalClick);
            BtnSelected.onClick.RemoveListener(OnBtnSelectedClick);
        }

        #endregion

        #region OnDestroy

        void OnDestroy() {
            RemoveEvents();
        }

        #endregion

    }

}