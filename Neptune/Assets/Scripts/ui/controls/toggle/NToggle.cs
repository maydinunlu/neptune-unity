﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
using System;

namespace neptune.ui.controls.toggle {

    public class NToggle : NControl {

        #region Event: NToggle

        public event Action<NToggle> NToggleEvent;

        #endregion


        #region Selected

        protected bool _selected;
        public bool Selected {
            get {
                return _selected;
            }
        }

        #endregion

        #region Interactible

        protected bool _interactible;
        public bool Interactible {
            get {
                return _interactible;
            }
        }

        #endregion

        /**************************************************/

        #region SetSelected -> OnSetSelected

        public void SetSelected(bool value, bool dispatchEvent = true) {
            _selected = value;

            OnSetSelected(value, dispatchEvent);

            if (NToggleEvent != null && dispatchEvent == true) {
                NToggleEvent(this);
            }
        }

        protected virtual void OnSetSelected(bool value, bool dispatchEvent) {
            // Override
        }

        #endregion

        #region SetInteractible -> OnSetInteractible

        public void SetInteractible(bool value) {
            _interactible = value;

            OnSetInteractible(value);
        }

        protected virtual void OnSetInteractible(bool value) {
            // Override
        }

        #endregion

    }

}