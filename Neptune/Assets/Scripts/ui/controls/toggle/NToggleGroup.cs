﻿/*  ----------------------------------------------------------------------------
 *  neptune-unity
 *  ----------------------------------------------------------------------------
 *  @maydinunlu
 *  ----------------------------------------------------------------------------
 *  Description:
 */
using System.Collections.Generic;

namespace neptune.ui.controls.toggle {

    public class NToggleGroup : NControl {

        public List<NToggle> ToggleList;

        #region SelectedToggle

        protected NToggle _selectedToggle;
        public NToggle SelectedToggle {
            get {
                return _selectedToggle;
            }
        }

        #endregion

        /**************************************************/

        #region SetSelectedToggle -> OnSetSelectedToggle

        public void SetSelectedToggle(NToggle toggle, bool value) {
            foreach (NToggle currentToggle in ToggleList) {
                currentToggle.SetSelected(!value, false);
            }

            _selectedToggle = toggle;
            _selectedToggle.SetSelected(value, false);

            OnSetSelectedToggle(toggle, value);
        }

        protected virtual void OnSetSelectedToggle(NToggle toggle, bool value) {
            // Override
        }

        #endregion

    }

}